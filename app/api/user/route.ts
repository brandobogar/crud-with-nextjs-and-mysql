import { PrismaClient } from "@prisma/client";

import { NextRequest, NextResponse } from "next/server";

const prisma = new PrismaClient();

//? Read All
export const GET = async (req: NextRequest) => {
  const users = await prisma.user.findMany({});
  return NextResponse.json({ users });
};

//? Create
export const POST = async (req: NextRequest) => {
  const { name, email } = await req.json();
  const user = await prisma.user.create({
    data: {
      name,
      email,
    },
  });

  return NextResponse.json({ user });
};

//? Delete
export const DELETE = async (req: NextRequest) => {
  const url = new URL(req.url).searchParams;
  const id = Number(url.get("id") || 0);

  const user = await prisma.user.delete({
    where: {
      id: id,
    },
  });

  if (!user) {
    return NextResponse.json({ message: "error" }, { status: 500 });
  }

  return NextResponse.json({});
};

//?Update

export const PUT = async (req: NextRequest) => {
  const { id, name, email } = await req.json();

  const user = await prisma.user.update({
    where: {
      id: Number(id),
    },
    data: {
      name,
      email,
    },
  });

  return NextResponse.json({ user });
};
