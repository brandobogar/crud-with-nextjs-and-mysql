"use client";

function page() {
  return (
    <>
      <div className="w-full h-screen flex items-start justify-center">
        <table className="table border-slate-300 w-2/3">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Email</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Brando</td>
              <td>brando@gmail.com</td>
              <td>Tes</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
}

export default page;
