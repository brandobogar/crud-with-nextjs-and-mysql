import UserCard from "@/components/elements/UserCard";
import AddUser from "@/components/modal/AddUser";
import UserList from "@/components/elements/UserList";
import EditUser from "@/components/modal/EditUser";
import Delete from "@/components/elements/Delete";

const getUsers = async () => {
  const res = await fetch(process.env.BASE_URL + "/api/user", {
    cache: "no-cache",
  });
  const json = await res.json();
  return json;
};

export default async function Home() {
  const users = await getUsers();

  return (
    <>
      <div className="flex flex-col justify-start mt-10 items-center w-full h-max  gap-y-8">
        <h1 className="text-3xl">User List</h1>
        <AddUser className="my-2" />
        <UserList>
          {users.users.map((user: any, index: number) => {
            return (
              <tr key={user.id}>
                <td>{index + 1}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>
                  <div className="flex flex-row gap-4">
                    <EditUser user={user} />
                    <Delete user={user} />
                  </div>
                </td>
              </tr>
            );
          })}
        </UserList>
      </div>
    </>
  );
}
