"use client";
import React, { useState } from "react";
import { user } from "@prisma/client";
import { useRouter } from "next/navigation";

interface Props {
  user: user;
}

function Delete({ user }: Props) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const handleDelete = async (id: number) => {
    setLoading(true);
    await fetch("/api/user?id=" + id, {
      method: "DELETE",
    });
    setLoading(false);
    router.refresh();
  };

  return (
    <button
      className="btn btn-error btn-sm"
      onClick={() => {
        handleDelete(user.id);
      }}
    >
      {loading ? (
        <span className="loading loading-spinner loading-sm"></span>
      ) : (
        "Del"
      )}
    </button>
  );
}

export default Delete;
