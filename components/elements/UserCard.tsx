"use client";

import { user } from "@prisma/client";
import { useRouter } from "next/navigation";
import { useState } from "react";
import EditUser from "../modal/EditUser";

interface Props {
  user: user;
}

function UserCard({ user }: Props) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleDelete = async (id: number) => {
    setLoading(true);
    await fetch("/api/user?id=" + id, {
      method: "DELETE",
    });
    setLoading(false);
    router.refresh();
  };
  return (
    <>
      <div
        key={user.id}
        className=" h-fit bg-yellow-50 border-2 border-slate-50 p-6 rounded-xl flex flex-row gap-4 card"
      >
        <div>
          <h1 className="text-xl card-title">User Card</h1>
          <div className="card-body p-0">
            <p>{user.name}</p>
            <p>{user.email}</p>
          </div>
        </div>
        <div className="flex gap-x-4 justify-end items-end w-1/2 card-actions">
          <EditUser user={user} />
          <button
            className="btn btn-error btn-sm"
            onClick={() => {
              handleDelete(user.id);
            }}
          >
            {loading ? (
              <span className="loading loading-spinner loading-sm"></span>
            ) : (
              "Del"
            )}
          </button>
        </div>
      </div>
    </>
  );
}

export default UserCard;
