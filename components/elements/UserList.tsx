"use client";

import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

function UserList({ children }: Props) {
  return (
    <>
      <table className="table border-slate-300 w-2/3 table-zebra table-sm">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{children}</tbody>
      </table>
    </>
  );
}

export default UserList;
