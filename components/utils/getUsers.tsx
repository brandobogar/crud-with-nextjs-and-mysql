export const getUsers = async () => {
  const res = await fetch(process.env.BASE_URL + "/api/user");
  const json = await res.json();
  return json;
};
