"use client";
import { useRouter } from "next/navigation";
import React, { useState } from "react";

interface Props {
  className: string;
}

const AddUser = ({ className }: Props) => {
  const [modal, setModal] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const router = useRouter();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await fetch("/api/user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        email: email,
      }),
    })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });

    setName("");
    setEmail("");
    router.refresh();
    setModal(false);
  };

  const handleChange = () => {
    setModal(!modal);
    console.log(modal);
  };
  return (
    <div className={className}>
      <button className="btn btn-neutral btn-sm" onClick={handleChange}>
        <span className="text-xs">Add User</span>
      </button>
      <input
        type="checkbox"
        className="modal-toggle"
        checked={modal}
        onChange={handleChange}
      />

      <div className="modal">
        <div className="modal-box">
          <h3>Add User</h3>
          <form className="form-control" onSubmit={handleSubmit}>
            <div>
              <label className="label">Name</label>
              <input
                type="text"
                placeholder="Enter your name"
                className="input input-bordered"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <label className="label">Email</label>
              <input
                type="text"
                placeholder="Enter your Email"
                className="input input-bordered"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <div className="modal-action">
              <button type="submit" className="btn btn-accent">
                Add
              </button>
              <button
                type="button"
                className="btn btn-error"
                onClick={() => {
                  setModal(!modal);
                }}
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddUser;
