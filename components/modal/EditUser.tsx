"use client";
import { useRouter } from "next/navigation";
import React, { use, useState } from "react";
import { user } from "@prisma/client";

interface Props {
  user: user;
}

const EditUser = ({ user }: Props) => {
  const [modal, setModal] = useState(false);
  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);
  const router = useRouter();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      await fetch("/api/user", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: user.id,
          name: name,
          email: email,
        }),
      })
        .then((res) => {
          console.log(res);
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }

    setModal(false);
    router.refresh();
  };

  const handleChange = () => {
    setModal(!modal);
    console.log(modal);
  };
  return (
    <div>
      <button className="btn btn-info btn-sm" onClick={handleChange}>
        <span className="text-xs">Edit</span>
      </button>
      <input
        type="checkbox"
        className="modal-toggle"
        checked={modal}
        onChange={handleChange}
      />

      <div className="modal">
        <div className="modal-box">
          <h3>Edit</h3>
          <form className="form-control" onSubmit={handleSubmit}>
            <div>
              <label className="label">Name</label>
              <input
                type="text"
                placeholder="Enter your name"
                className="input input-bordered"
                value={String(name)}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <label className="label">Email</label>
              <input
                type="text"
                placeholder="Enter your Email"
                className="input input-bordered"
                value={String(email)}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <div className="modal-action">
              <button type="submit" className="btn btn-accent">
                Add
              </button>
              <button
                type="button"
                className="btn btn-error"
                onClick={() => {
                  setModal(!modal);
                }}
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditUser;
